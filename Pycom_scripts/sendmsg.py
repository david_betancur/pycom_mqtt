from network import WLAN      # For operation of WiFi network
import time                   # Allows use of time.sleep() for delays
import pycom                  # Base library for Pycom devices
from umqtt import MQTTClient  # For use of MQTT protocol to talk to MQTTCLOUD
import ubinascii              # Needed to run any MicroPython code
import machine                # Interfaces with hardware components
import micropython            # Needed to run any MicroPython code

# INICIO CONFIGURACIONES
# These need to be change to suit your environment
RANDOMS_INTERVAL = 100 # tiempo entre envío y envío (milisegundos)
last_random_sent_ticks = 0  # milisegundos

# Credenciales Red Wifi
WIFI_SSID = "MOVISTAR WIFI1030"
WIFI_PASS = "" # "" si no requiere clave

# Credenciales de MQTT cloud
SERVER = "m11.cloudmqtt.com"
PORT = 15681 # El puerto (PORT)
USER = "cchffnkr"
KEY = "WMrgC9_cfSvG"
CLIENT_ID = ubinascii.hexlify(machine.unique_id())  # Puede ser cualquier cosa, se dejo esto porque venía con el ejemplo
TOPIC_1 = "hacia_placa"
TOPIC_2 = "desde_placa"
# FIN CONFIGURACIONES

# conexión al WIFI
wlan = WLAN(mode=WLAN.STA)
wlan.connect(WIFI_SSID, auth=(WLAN.WPA2, WIFI_PASS), timeout=5000)

while not wlan.isconnected():    # Se queda hasta que se logre conectar
    machine.idle()

print("Connected to Wifi")
pycom.rgbled(0xffd7000) # Naranjado: se logró conectar al wifi



# FUNCIONES

# FUNCION QUE SE CORRE CUANDO LLEGA UN MENSAJE DE MQTTCLOUD
def new_msg(topic, msg):
    print((topic, msg))
    if msg == b"ON":             # si el mensaje dice ON...
        pycom.rgbled(0xffffff)   # ... se prende el LED
    elif msg == b"OFF":          # Si el pensaje dice "OFF" ...
        pycom.rgbled(0x000000)   # ... el LED se apaga
    else:                        # Si se recibió algo mas ...
        print("Unknown message: ")
        print(msg)

# FUNCION PARA LEER UN PIN ADC EN ESPECIFICO
def read_adc_pin(adc_pin):
    adc = machine.ADC()             # Crea el objeto de adc
    apin = adc.channel(pin=adc_pin)   # le asigna el pin especificado
    val = apin() # valor de voltaje del pin (0 - 1025)
    return val # devuelve el valor

# FUNCION QUE ENVIA ALGO A MQTTCLOUD
def send_msg(msg_to_send):
    global last_random_sent_ticks
    global RANDOMS_INTERVAL

    if ((time.ticks_ms() - last_random_sent_ticks) < RANDOMS_INTERVAL):
        return; # Too soon since last one sent.

    print("Publicando: {0} al topic {1} ... ".format(msg_to_send, TOPIC_2), end='')
    try:
        client.publish(topic=TOPIC_2, msg=str(msg_to_send))
        print("DONE")
    except Exception as e:
        print("FAILED")
    finally:
        last_random_sent_ticks = time.ticks_ms()

# Crea el cliente de MQTTCLOUD
client = MQTTClient(CLIENT_ID, SERVER, PORT, USER, KEY)

client.set_callback(new_msg) # callback para la llegada de nuevos mensajes
client.connect()
client.subscribe(TOPIC_1) # se subscribe al TOPIC_1 que es hacia placa para leer todo lo que llega con ese topic
print("Conectado al servidor: %s, y subscrito al topic: %s" % (SERVER, TOPIC_1))

pycom.rgbled(0x00ff00) # LED verde significa que se conecto a MQTTcloud

try:                   # recibe y manda mensajes hasta que haya un error
    while 1:
        client.check_msg()# Lee un mensaje si existe
        mensaje = read_adc_pin('P16') # lee el pin P16 analogico
        send_msg(msg_to_send=mensaje)     # manda el valor que se leyó en el pin
finally:                  # Si hay un error...
    client.disconnect()   # se desconecta
    client = None
    wlan.disconnect()
    wlan = None
    pycom.rgbled(0x000022)# LED azul: desconectado
    print("Desconectado de MQTTCLOUD")
