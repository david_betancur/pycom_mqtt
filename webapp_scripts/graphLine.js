// Programa para graficar en google charts lo que llega a MQTTcloud


google.charts.load('current', {
    'packages': ['corechart']
});
google.charts.setOnLoadCallback(drawChart);

var myMsg = 0; // where I put my message
var varaux = 0;
var indice = 0;
// gauge variables.

// Google Charts Stuff
function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['tiempo', 'Valor'],
        [indice, 0]
    ]);

    var options = {
        title: 'Valor Pin ADC',
        curveType: 'function',
        legend: {
            position: 'bottom'
        }
    };

    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
    chart.draw(data, options);

    setInterval(function() {
        if (varaux == 1) {
            indice = indice + 1;
            data.addRow([indice, Number(myMsg)]);
            chart.draw(data, options);
            varaux = 0;
        }
    }, 100);
}

var clientId = "ws" + Math.random();
client = new Paho.MQTT.Client("m11.cloudmqtt.com", 35681, clientId);

// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;
var options = {
    useSSL: true,
    userName: "cchffnkr",
    password: "WMrgC9_cfSvG",
    onSuccess: onConnect,
    onFailure: doFail
}

// connect the client
client.connect(options);

// called when the client connects
function onConnect() {
    // Once a connection has been made, make a subscription and send a message.
    document.getElementById("connstatus").innerHTML = "Connected";
    console.log("onConnect");
    client.subscribe("#"); // client.subscribe("#"); Para subscribirse a todos los topics
}

// se llama cuando hay un error conectandose
function doFail(e) {
    console.log("error")
    console.log(e);
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
    document.getElementById("connstatus").innerHTML = "Not Connected";
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
    }
}

// called when a message arrives
function onMessageArrived(message) {
    console.log("mensaje")
    myMsg = message.payloadString;
    varaux = 1;
    console.log(myMsg);
}
